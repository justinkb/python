# Copyright 2008-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'PyQt4-4.4.4-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

myexparam sip_version

MY_PNV="PyQt4_gpl_x11-${PV%.0}"
require pypi
require sourceforge [ project=pyqt suffix=tar.gz ]
require python [ blacklist=none multiunpack=true work=${MY_PNV} ]

SUMMARY="PyQt4 is a set of Python bindings for the Qt4 toolkit"
DESCRIPTION="
PyQt is a set of Python bindings for Nokia's Qt application framework and runs
on all platforms supported by Qt including Windows, MacOS/X and Linux. There are
two sets of bindings: PyQt v4 supports Qt v4; and the older PyQt v3 supports Qt v3
and earlier. The bindings are implemented as a set of Python modules and contain
over 300 classes and over 6,000 functions and methods.
"
BASE_URI="http://www.riverbankcomputing.co.uk"
HOMEPAGE="${BASE_URI}/software/pyqt/intro/"

#BUGS_TO="philantrop@exherbo.org"

UPSTREAM_CHANGELOG="${BASE_URI}/static/Downloads/${PN}/ChangeLog"
UPSTREAM_RELEASE_NOTES="${BASE_URI}/news/pyqt-$(ever major)$(ever range 2)"
UPSTREAM_DOCUMENTATION="${BASE_URI}/static/Docs/${PN}/html/classes.html [[ lang = en ]]"

SLOT="0"
LICENCES="GPL-3"
MYOPTIONS="dbus debug opengl sql
           multimedia [[ description = [ Build QtMultimedia bindings ] ]]"

DEPENDENCIES="
    build+run:
        dev-python/sip[>=$(exparam sip_version)][python_abis:*(-)?]
        x11-libs/qt:4[>=4.7][X(+)][dbus?][multimedia(-)?][opengl?][qt3support][sql?]
        dbus? ( dev-python/dbus-python )
"

DEFAULT_SRC_INSTALL_PARAMS=( INSTALL_ROOT="${IMAGE}" )

pyqt_enable() {
    local flag=${1,,} module=${2:-Qt${1}}

    if option "${flag}" ; then
        echo "--enable=${module}"
    fi
}

# We don't install Phonon bindings
PYQT_CONFIGURE_OPTION_ENABLES=( DBus Multimedia OpenGL Sql )
PYQT_CONFIGURE_ALWAYS_ENABLES=( QtAssistant QtCore QtDeclarative QtGui QtHelp QtNetwork QtScript QtScriptTools QtSvg QtTest QtXml
    QtXmlPatterns pyqtconfig )
PYQT_CONFIGURE_PARAMS=(
        # Disable PyQt API file for QScintilla
        --no-qsci-api
        # Disable QtDesigner support. Enable with: --enable=QtDesigner --enable=uic
        --no-designer-plugin )

prepare_one_multibuild() {
    python_prepare_one_multibuild

    if option !dbus; then
        edo sed -e '/pkg-config.*dbus-1/s/sout.*$/return # DBus support disabled/' -i configure.py
    else
    # It uses banned pkg-config when looking for dbus-python.
    edo sed -e 's/"pkg-config/"'"${PKG_CONFIG}"'/' -i configure.py
    fi

    # When system python is set to 2.6 python_bytecompile() can't process
    # source files that use python 3 syntax. See Gentoo bug #274499.
    [[ $(python_get_abi) == 2.* ]] && edo rm -rf pyuic/uic/port_v3
    [[ $(python_get_abi) == 3.* ]] && edo rm -rf pyuic/uic/port_v2
}

configure_one_multibuild() {
    edo ${PYTHON} configure.py \
        $(option debug && echo '--debug') \
        --confirm-license \
        --bindir /usr/$(exhost --target)/bin \
        --destdir $(python_get_sitedir) \
        --sipdir /usr/share/sip \
        "${PYQT_CONFIGURE_PARAMS[@]}" \
        $(for s in ${PYQT_CONFIGURE_ALWAYS_ENABLES[@]} ; do echo "--enable=${s}" ; done) \
        $(for s in "${PYQT_CONFIGURE_OPTION_ENABLES[@]}" ; do pyqt_enable ${s} ; done)

    edo find "${WORK}" -name Makefile | xargs sed -i "/^\tstrip /d"
}

install_one_multibuild() {
    default
    python_bytecompile

    insinto /usr/share/doc/${PNVR}/html
    doins -r doc/html/*

    insinto /usr/share/doc/${PNVR}
    doins -r examples

    if [[ -d "${IMAGE}"/usr/lib ]]; then
        edo cp -a "${IMAGE}"/usr/lib/* "${IMAGE}"/usr/$(exhost --target)/lib/
        edo rm -r "${IMAGE}"/usr/lib
    fi

    # Kill empty dirs.
    edo find "${IMAGE}"/usr/ -type d -empty -delete
}

