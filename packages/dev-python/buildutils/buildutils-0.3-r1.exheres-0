# Copyright 2008, 2009 Ali Polatel
# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'buildutils-0.3.ebuild' from Gentoo, which is:
#   Copyright 1999-2008 Gentoo Foundation

require pypi
require setup-py [ import='setuptools' blacklist=3 ]

HOMEPAGE="http://buildutils.lesscode.org/"

SUMMARY="Extensions for developing Python libraries and applications."
DESCRIPTION="
Python Build Utilities (buildutils) is a set of extension commands to python's
standard distutils that are often useful during development and deployment of
python projects. buildutils was created with the desire of removing make and
other external build tools from the python development process.
"

LICENCES="MIT"

PLATFORMS="~amd64 ~x86"

SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        dev-python/py[python_abis:*(-)?]
        doc? ( dev-python/pudge[python_abis:*(-)?] )
"

# TODO tests need love
RESTRICT="test"

compile_one_multibuild() {
    setup-py_compile_one_multibuild
    if option doc ; then
        # pudge command is disabled by default, enable it.
        edo ${PYTHON} setup.py addcommand -p buildutils.pudge_command
        edo ${PYTHON} setup.py pudge || die "generating docs failed"
    fi
}

install_one_multibuild() {
    setup-py_install_one_multibuild
    if option doc; then
        insinto /usr/share/doc/${PNVR}/html
        doins doc/html/*
    fi
}

test_one_multibuild() {
    # pytest command is disabled by default, enable it.
    edo ${PYTHON} setup.py addcommand -p buildutils.pytest_command
    edo ${PYTHON} setup.py pytest || die "tests failed"
}

