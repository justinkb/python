# Copyright 2013 Jorge Aparicio
# Distributed under the terms of the GNU General Public License v2

require pypi
require setup-py [ import=setuptools test=pytest ]

SUMMARY="The modular source code checker: pycodestyle, pyflakes and McCabe"
HOMEPAGE+=" https://gitlab.com/pycqa/${PN}"
UPSTREAM_DOCUMENTATION="http://${PN}.readthedocs.org"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"

DEPENDENCIES="
    build+run:
        dev-python/entrypoints[>=0.3&<0.4.0][python_abis:*(-)?]
        dev-python/mccabe[>=0.6.0&<0.7.0][python_abis:*(-)?]
        dev-python/pycodestyle[>=2.5.0&<2.6.0][python_abis:*(-)?]
        dev-python/pyflakes[>=2.1.0&<2.2.0][python_abis:*(-)?]
        python_abis:2.7? (
            dev-python/configparser[python_abis:2.7]
            dev-python/enum34[python_abis:2.7]
            dev-python/functools32[python_abis:2.7]
            dev-python/typing[python_abis:2.7]
        )
    test:
        dev-python/mock[>=2.0.0][python_abis:*(-)?]
"

test_one_multibuild() {
    local test_dir="${TEMP}/home/$(python_get_abi)"
    export PYTHONPATH="${test_dir}/lib/python"

    # Install to register entry-points for built-in plugins
    edo mkdir -p "${PYTHONPATH}"
    edo ${PYTHON} -B setup.py install --home="${test_dir}"
    edo ${PYTHON} -B -m pytest tests

    unset PYTHONPATH
}

