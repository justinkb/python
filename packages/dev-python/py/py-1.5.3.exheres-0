# Copyright 2008, 2009 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'py-0.9.1.ebuild' from Gentoo, which is:
#   Copyright 1999-2008 Gentoo Foundation

require pypi setup-py [ import=setuptools ] utf8-locale

SUMMARY="Library with cross-python path, ini-parsing, io, code, log facilities"
DESCRIPTION="
The py lib is a Python development support library featuring the following tools and modules:

* py.path: uniform local and svn path objects
* py.apipkg: explicit API control and lazy-importing
* py.iniconfig: easy parsing of .ini files
* py.code: dynamic code generation and introspection
"

BUGS_TO="alip@exherbo.org"

UPSTREAM_CHANGELOG="https://pylib.readthedocs.org/en/latest/changelog.html"
UPSTREAM_DOCUMENTATION="https://pylib.readthedocs.org/en/latest"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    post:
        dev-python/pytest[python_abis:*(-)?] [[ note = [ actually test ] ]]
"

pkg_setup() {
    # for tests
    require_utf8_locale
}

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    # All tests of that file fail
    edo rm testing/log/test_warning.py
}

test_one_multibuild() {
    # avoid a py <--> pytest dependency loop
    if has_version dev-python/pytest[python_abis:$(python_get_abi)] ; then
        PYTEST="py.test-$(python_get_abi)"
        PYTHONPATH="$(ls -d ${PWD}/build/lib*)" edo ${PYTEST} "${PYTEST_PARAMS[@]}"
    else
        ewarn "dev-python/pytest[python_abis:$(python_get_abi)] not yet installed, skipping tests"
    fi
}

