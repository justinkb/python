# Copyright 2012-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools test=pytest ]

SUMMARY="wxPython is a wxGTK-based GUI toolkit for Python"
HOMEPAGE="https://www.wxpython.org"

LICENCES="LGPL-2"
SLOT="4.0"
PLATFORMS="~amd64"
MYOPTIONS="opengl"

# some tests are broken, last checked: 4.0.6
RESTRICT="test"

# TODO: Bundles sip, Fedora has a patch
# dev-python/sip[>=4.19.1][python_abis:*(-)?]
DEPENDENCIES="
    build:
        dev-python/pathlib2[python_abis:*(-)?]
        virtual/pkg-config
    build+run:
        dev-python/six[python_abis:*(-)?]
        dev-python/numpy[python_abis:*(-)?]
        dev-python/Pillow[python_abis:*(-)?]
        x11-libs/wxGTK:=[>=3.0.2][tiff]
        opengl? ( dev-python/PyOpenGL[python_abis:*(-)?] )
    test:
        dev-python/appdirs[python_abis:*(-)?]
        dev-python/numpy[python_abis:*(-)?]
        dev-python/Pillow[python_abis:*(-)?]
"

SETUP_PY_SRC_COMPILE_PARAMS=(
    WXPORT=gtk3
)
SETUP_PY_SRC_INSTALL_PARAMS=(
    WXPORT=gtk3
)

prepare_one_multibuild() {
    # use prefixed pkg-config
    edo sed \
         -e "s:pkg-config :$(exhost --tool-prefix)& :" \
         -i buildtools/config.py \
         -i wscript

    setup-py_prepare_one_multibuild
}

src_compile() {
    export PKG_CONFIG=$(type -P ${PKG_CONFIG})

    setup-py_src_compile
}

