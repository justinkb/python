# Copyright 2008 Ali Polatel
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'twisted.eclass' from Gentoo, which is:
#   Copyright 2005 Gentoo Foundation
#
# Purpose: To aid installing and testing twisted packages.
#
# required:
# TWISTED_MODULE_NAMES (array) = python module name (e.g. twisted.web)
#
# optional:
# TWISTED_DISABLE_TESTS (array) = power of 2 ( test1 test/test_test1.py test2 test/test_test2.py ... )

require setup-py [ import='distutils' blacklist='3' ]

HOMEPAGE="http://www.twistedmatrix.com/"
DOWNLOADS="http://twistedmatrix.com/Releases/${PN/Twisted/}/$(ever range 1-2)/${PNV}.tar.bz2"

LICENCES="MIT"

DESCRIPTION="
Twisted is an event-driven networking engine written in Python and licensed
under the MIT license.
Twisted projects variously support TCP, UDP, SSL/TLS, multicast, Unix sockets, a
large number of protocols (including HTTP, NNTP, IMAP, SSH, IRC, FTP, and
others), and much more.
Projects:
Twisted Core:
    Asynchronous event loop.
Twisted Conch:
    An SSH and SFTP protocol implementation together with clients and servers.
Twisted Web:
    An HTTP protocol implementation together with clients and servers.
Twisted Web2:
    An HTTP/1.1 Server Framework (Experimental).
Twisted Mail:
    An SMTP, IMAP and POP protocol implementation together with clients and servers.
Twisted Names:
    A DNS protocol implementation with client and server.
Twisted News:
    An NNTP protocol implementation with client and server.
Twisted Words:
    Chat and Instant Messaging.
Twisted Trial:
    Unittest-compatible automated testing.
Twisted Lore:
    Documentation generator with HTML and LaTeX support.
Twisted Runner:
    Process management, including an inetd server.
"
BUGS_TO="alip@exherbo.org"
REMOTE_IDS="pypi:Twisted"
UPSTREAM_CHANGELOG=""
UPSTREAM_RELEASE_NOTES="http://twistedmatrix.com/trac/browser/trunk/NEWS"

SLOT="0"

twisted_test_one_multibuild() {
    # only needed for non-core packages
    if ! [[ ${PN} == "TwistedCore" ]]; then
        # This is a hack to make tests work without installing them
        edo cp -R "${ROOT}$(python_get_sitedir)/twisted/" "${TEMP}/"

        # We have to get rid of the existing version of this package
        for module in "${TWISTED_MODULE_NAMES[@]}"; do
            edo rm -rf "${TEMP}/twisted/${module}/twisted./}/"
        done
    fi

    # install the module to TEMP
    edo "${PYTHON}" setup.py install --root="${TEMP}" --no-compile --force --install-lib="/"

    edo pushd "${TEMP}"

    # disable tests
    for ((i=0;i<${#TWISTED_DISABLE_TESTS[*]};i++)); do
        edo sed -e "s/${TWISTED_DISABLE_TESTS[$i]}/_&/g" \
            -i twisted/${TWISTED_DISABLE_TESTS[((i+=1))]}
    done

    # run the tests
    for test in "${TWISTED_MODULE_NAMES[@]}"; do
        echo "Testing: ${test}"
        PATH="${TEMP}/usr/bin:${PATH}" PYTHONPATH="." edo trial \
            --temp-directory="tests" "${test}"
    done

    edo popd
}

test_one_multibuild() {
    twisted_test_one_multibuild
}

twisted_install_one_multibuild() {
    setup-py_install_one_multibuild

    if [[ -d doc/man ]]; then
        doman doc/man/*.1
    fi

    if [[ -d doc ]]; then
        insinto /usr/share/doc/${PNVR}
        doins -r $(find doc -mindepth 1 -maxdepth 1 -not -name man)
    fi
}

install_one_multibuild() {
    twisted_install_one_multibuild
}

